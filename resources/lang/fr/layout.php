<?php

return [
    'intro' => 'Bienvenue sur l\'espace citoyen de<br><b>Amal wa 3amal</b>',
    'make_application' => 'Déposez une demande',
    'make_app_description' => 'Veuillez sélectionner votre circonscription électorale afin de vous diriger vers votre député . ',
    'follow_request' => 'Suivez votre demande',
    'roles' => 'en appuyant sur accepter  vous acceptez que conformément à l’article 145 de règlement intérieur de l\'assemblé des représentant du peuple, Les données qui concernent une demande, après étude et filtrage faites par le député, peuvent être transférées à un membre du gouvernement sous forme d’une question écrite et que les questions et réponses seront rendus publiques et publiées.',
    'accepte' => 'Accepter',
    'refuse' => 'Refuser',
    'intro_description_1' => '<br>Le mouvement Amal wa amal  met à votre disposition ce site web afin de vous faciliter la communication avec vos représentants à l\'Assemblée des représentants du Peuple et le suivi de l\'évolution du traitement de vos problèmes.
                            <br>',
    'intro_description' => '<div style="color: #454545;">
                    Nous sommes heureux de vous souhaiter la bienvenue sur notre  espace Citoyen, destiné à faciliter vos démarches liées à vos demandes et plaintes .<br>
                    Cet espace est accessible 24 heures sur 24, 7 jours sur 7 depuis un ordinateur, un smartphone ou une tablette:
                    <br>
                    Vous pourrez ainsi  utiliser les fonctionnalités suivantes :
                    <ul>
                        <li>
                            <i class="fa fa-check" aria-hidden="true" style="color: #2ecc71"></i>
                            Deposer une demande ou une plainte au député qui représente votre circonscription
                        </li>
                        <li>
                            <i class="fa fa-check" aria-hidden="true" style="color: #2ecc71"></i>
                            Consulter l’historique de vos demandes
                        </li>
                        <li>
                            <i class="fa fa-check" aria-hidden="true" style="color: #2ecc71"></i>
                            Suivre l’état de traitement de vos demandes
                        </li>
                        <li>
                            <i class="fa fa-check" aria-hidden="true" style="color: #2ecc71"></i>
                            Disposer d’un espace de stockage sécurisé pour vos pièces justificatives
                        </li>
                    </ul>
                    Nos services sont à votre disposition pour tout renseignement.
                    </p>
                </div>',

    'id' => 'CIN',
    'name' => 'Nom et prénom',
    'sex' => 'Genre',
    'select_sex' => 'Sélectionner votre sexe',
    'male' => 'Homme',
    'female' => 'Femme',
    'phone_number' => 'Numéro de téléphone',
    'email' => 'Email',
    'category' => 'Secteur',
    'title' => 'Objet',
    'title_placeholder' => '',
    'description' => 'Description',
    'description_placeholder' => '',
    'password' => 'Code Ticket',

    'circonscription' => 'Circonscription',
    'send' => 'Envoyer',

    'join_files' => 'Pieces jointes',
    'select_category' => 'Sélectionner votre secteur',

    'select_your' => '-- Sélectionnez la votre --',
    'create_request' => 'CRÉEZ VOTRE DEMANDE',
    'deput_mombers' => 'Depute et membre du mouvement <b>Amal & 3amal</b>',
    'yassine_ayari' => 'Yassine Ayari',
    'imen_bettaieb' => 'Imen Bettaieb',

    'page_404' => 'Oups !',
    'not_found' => 'La page n’existe pas ou n’est pas disponible.',
    'go_to_home' => 'Retourner sur la page d\'accueil',

    'follow' => 'Suivre',

    'start' => 'début',
    'tell_us_about_you' => 'Parlez nous de vous',
    'tell_us_about_request' => 'Parlez nous de votre demande',

    'our_deputies_at' => 'Nos députés à',
    'proof_of_contact' => 'Preuve de contact',

    'about' => 'A propos',
    'loading' => 'Chargement...',
    'district' => 'Secteur électoral',
    'transfert_files' => 'Transférer des fichiers',

    'p1' => 'Nous n\'avons pas de députés à',
    'p2' => 'Merci de contacter l\'un des deputès élus dans votre circonscription électorale afin qu\'il puisse vous aider',

    'sms_message' => 'Votre demande a eté enregistré, pour suivre votre demande CIN: :cin Code Ticket: :key',
    'registered_message' => 'Votre demande a eté enregistré, pour suivre votre demande <br><b>CIN</b>: :cin <br><b>Code Ticket</b>: :key',

    'email_subscription' => 'Souscription',
    'email_contact' => 'pour nous contacter',
    'email_title' => 'Demande enregistrée',
    'email_message' => '
            Bonjour :user_name
    <p>Votre demande a été enregistrée</p>
    <p>Nous étudions actuellement cette demande et vous pouvez la suivre directement via  <a style="text-decoration: underline;font-weight: bold;" href="https://question.mouvementamal.tn/fr/request?cin=:cin&password=:key" target="_blank">ce lien</a></p>

    <p>ou via <a style="text-decoration: underline;font-weight: bold;" href="https://question.mouvementamal.tn/fr/request" target="_blank">l\'application</a> en entrant les informations suivantes :</p>
    <p>  CIN :cin Code secret :key</p>
    <br /> <br />
    Cordialement.
    ',

    'select_district' => 'Séléctionner votre circonscription',
    'gov_requested' => 'Gov Requested',
    'text_gov_requested' => 'Ce site web est accessible, 24 heures sur 24  tout au long de la semaine, à partir de votre ordinateur, smartphone, ou tablette.
Vous serez capable d\'utiliser les fonctionnalités suivantes :
Soumettre une proposition, une demande ou une plainte au représentant correspondant à votre circonscription
Suivre l\'évolution du traitement de vos requêtes',
    'email_subject' => 'Demande enregistrée',

    'confidentialityTitle' => 'Conditions d’utilisation',

    'copyright' => '© Copyright 2020 MouvementAmal',

    'confidentiality' => '<div style="color: #454545;">
    <p>
En ayant accès au Site question.mouvementamal.tn, vous confirmez votre compréhension des Conditions générales d\'utilisation et consentez à les respecter, si vous n\'acceptez pas les présentes Conditions générales d\'utilisation, nous vous prions de ne pas utiliser le Site. <br>
Le Site se réserve la faculté à tout moment, de changer, modifier, ajouter ou supprimer tout ou partie des dispositions des présentes Conditions générales d\'utilisation.<br>
<br>
Utilisation légale
<br>
<br>
Lorsque vous créez une demande sur le Site, vous serez tenu de nous fournir un certain nombre d\'informations.<br>
Vous devrez fournir des informations vraies, précises, actuelles et complètes vous concernant, requises par Mouvement Amal wa Aamal dans le formulaire de création des demandes.<br>
Vous êtes responsable du maintien de la confidentialité, de la restriction à l\'accès à vos demandes et acceptez d\'assumer la responsabilité pour toutes les activités effectuées.<br>
Vous ne pouvez à aucun moment utiliser l\'accès d\'un tiers, sans l\'accord expresse de son titulaire.<br>
Vous acceptez que nous puissions communiquer avec vous par le biais de la messagerie électronique (e-mail), SMS et par téléphone.<br>
Vous êtes responsable de veiller à ce que votre utilisation du Site soit conforme aux présentes Conditions d\'Utilisation et selon le droit applicable et les pratiques généralement acceptées. <br>
<br>
La violation de ces Conditions d\'Utilisation
<br>
<br>
S\'il apparaît que votre utilisation du Site enfreint les présentes Conditions d\'Utilisations, ou lorsque Mouvement Amal wa Aamal a des motifs raisonnables de croire que cette violation a eu lieu, Mouvement Amal wa Aamal se réserve le droit de fermer ou de bloquer temporairement l\'accès au Site immédiatement et sans avertissement préalable. <br>
<br>
Accessibilité
<br>
<br>
Votre accès au site peut, de temps à autre, être totalement ou partiellement inaccessible en raison de la maintenance du système ou pour d\'autres raisons. Dès que possible, nous vous fournirons des informations sur toutes les limitations sur l\'accessibilité.  <br>
<br>
<br>
Comment nous contacter
Si vous avez des questions ou des commentaires au sujet de ces Conditions d\'Utilisation, veuillez nous contacter à l\'adresse suivante : contact@mouvementamal.com<br>
<br>


Politique de confidentialité
<br>
<br>
En tant qu\'utilisateur de l’application, vous bénéficiez, conformément à Loi organique n° 2004-63 en date du 27 juillet 2004 portant sur la protection des données à caractère personnel, d\'un droit d\'accès, d\'opposition et de rectification des données personnelles qui vous concernent et que vous êtes susceptible de communiquer dans le cadre de l\'utilisation du site.<br>
 Les Données que vous nous communiquer au moment de la création d’une demande, ou lors de la mise à jour de celle-ci, sont collectées et traitées par Mouvement Amal wa Aamal conformément à la règlementation en vigueur.<br>
 <br>
Données personnelles
<br>
<br>
Les données que vous nous donnez :<br>
Numéro de la carte d\'identité nationale, c\'est l\'identifiant unique pour suivre les demandes.<br>
Le nom et le prénom : sont utilisés pour identifier les demandeurs.<br>
Adresse mail : email pour recevoir les notifications liées à vos demandes. <br>
Numéro de téléphone : pour recevoir les SMS contenant le code d\'accès pour le suivi des demandes.<br>
Les documents justificatifs : utilisés pour avoir le détail des demandes ainsi que les justificatifs, qui peuvent être transférés aux ministres concernés.<br>
Ces données sont utilisées par Mouvement Amal wa Aamal afin d’étudier la demande, la traiter au près des service concernés. Tous ces données seront détruites après 3 mois de la clôture de la demande.<br>
Les données recueillies lorsque vous utilisez le site :<br>
Informations sur votre appareil, la connexion à nos services et l\'utilisation multi-appareils. Ces informations peuvent entre autres inclure le système d\'exploitation, la version du navigateur Web, les adresses IP<br>
Informations sur la façon dont vous utilisez le site. Nous enregistrons lorsque vous vous connectez ou déconnectez de votre compte <br>
Ces données sont utilisées par Mouvement Amal wa Aamal afin de sécuriser le site et d’améliorer le service<br>
<br>
Pourquoi nous traitons et partageons vos informations ?<br>
Conformément à l’article 145 de règlement intérieur de l\'assemblé des représentants du peuple, les données qui concernent une demande, après étude et filtrage faites par le député, peuvent être transférées à un membre du gouvernement sous forme d’une question écrite, les questions et les réponses seront publiées.<br>
 <br>
Enfin, Mouvement Amal wa Aamal, a procédé à la déclaration de traitement des données personnelles conformément à la loi organique n°2004-63 en date du 27 juillet 2004 portant sur la protection des données à caractère personnel. A cet effet, Mouvement Amal wa Aamal s’engage à protéger la confidentialité de vos Données.<br>
Afin d’exercer vos droits d’accès, de rectification et d’opposition, vous pouvez envoyer votre demande à contact@mouvementamal.tn<br>
</p>
</div>
',
    'Written_question' => 'سؤال كتابي',
    'Oral_question' => 'سؤال شفاهي',
    'Direct_messaging' => 'مراسلة مباشرة',
    'Answer_to_a_written_question' => 'جواب على سؤال كتابي',
];
