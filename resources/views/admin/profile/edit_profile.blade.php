@extends('admin.layouts.app')

@section('section_title')
    Edit Profile
@endsection

@section('content')

    <div class="row">
        <div class="col-md-6">
            @include('errors.errors')
            <form method="POST" action="{{ route('update_profile', [$profile->id]) }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label for="first_name" class="col-md-5 col-form-label text-md-right">
                        {{ __('District') }} *
                    </label>
                    <div class="col-md-6">
                        <select class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}"
                                name="district"
                                required
                        >
                            <option>- Sélection votre Circonscription -</option>
                            @foreach($districts as $district)
                                <option value="{{ $district->id }}"
                                        @if($district->id == $profile->district_id) selected @endif>{{ $district->name_en }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="avatar" class="col-md-5 col-form-label text-md-right">
                        {{ __('Avatar') }} *
                    </label>
                    <div class="col-md-6">
                        @if($profile->avatar)
                            <img src="{{ asset($profile->avatar) }}" class="img-thumbnail" style="max-width: 200px;">
                            <br>
                            <a href="{{ route('remove_profile_avatar', [$profile->id]) }}" class="btn btn-danger"
                               style="margin-top: 10px;">Remove</a>
                        @else
                            <input id="avatar"
                                   type="file"
                                   name="avatar"
                                   value="{{ old('avatar') }}" required>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name_ar" class="col-md-5 col-form-label text-md-right">
                        {{ __('Profile Name AR') }} *
                    </label>
                    <div class="col-md-6">
                        <input id="name_ar"
                               type="text"
                               class="form-control{{ $errors->has('name_ar') ? ' is-invalid' : '' }}"
                               name="name_ar"
                               value="{{ old('name_ar')?old('name_ar'):$profile->name_ar }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name_fr" class="col-md-5 col-form-label text-md-right">
                        {{ __('Profile Name FR') }} *
                    </label>
                    <div class="col-md-6">
                        <input id="name_fr"
                               type="text"
                               class="form-control{{ $errors->has('name_fr') ? ' is-invalid' : '' }}"
                               name="name_fr"
                               value="{{ old('name_fr')?old('name_fr'):$profile->name_fr }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name_en" class="col-md-5 col-form-label text-md-right">
                        {{ __('Profile Name EN') }} *
                    </label>
                    <div class="col-md-6">
                        <input id="name_fr"
                               type="text"
                               class="form-control{{ $errors->has('name_fr') ? ' is-invalid' : '' }}"
                               name="name_fr"
                               value="{{ old('name_fr')?old('name_fr'):$profile->name_fr }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email"
                           class="col-md-5 col-form-label text-md-right">{{ __('E-Mail Address *') }}</label>
                    <div class="col-md-6">
                        <input id="email" type="email"
                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                               name="email" value="{{ old('email')?old('email'):$profile->email }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone_number"
                           class="col-md-5 col-form-label text-md-right">{{ __('Phone number *') }}</label>
                    <div class="col-md-6">
                        <input id="phone_number" type="text"
                               class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}"
                               name="phone_number"
                               value="{{ old('phone_number')?old('phone_number'):$profile->phone_number }}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="active" class="col-md-5 col-form-label text-md-right">{{ __('Active *') }}</label>
                    <div class="col-md-6">
                        <select id="active" class="form-control{{ $errors->has('active') ? ' is-invalid' : '' }}"
                                name="active">
                            <option value="1" {{ ($profile->active==1)?'selected':'' }}>Active</option>
                            <option value="0" {{ ($profile->active==0)?'selected':'' }}>Not Active</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 offset-5">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Edit Profile') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
