@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.css" rel="stylesheet"
          type="text/css"/>
@endsection

@section('section_title')
    List data previleges
@endsection

@section('content')

    @include('errors.errors')
    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif
    <table id="table" class="table table-striped table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th style="width: 150px;">Created at</th>
            <th>Action</th>
        </tr>
        </thead>
    </table>

    <div class="text-right" style="margin-top: 20px;">
        <a href="{{ route('create_user_type') }}" class="btn btn-primary">Add New user type</a>
    </div>

@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#table').DataTable({
                order: [0, 'desc'],
                processing: true,
                serverSide: true,
                ajax: "{{ url('/api/admin/users_types_list') }}",
                columns: [
                    {"data": "id"},
                    {"data": "name"},
                    {
                        "data": "created_at",
                        "searchable": false,
                        "orderable": false,
                    },
                    {
                        "data": "action",
                        "searchable": false,
                        "orderable": false,
                    }
                ]
            });

        });

    </script>

@endsection
