@extends('admin.layouts.app')

@section('after_style')
    <link href="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('section_title')
    Categories
@endsection

@section('content')

    @include('errors.errors')
    @if($flash = session('message'))
        <div class="alert alert-success">
            <i class="fa fa-bell" aria-hidden="true"></i>
            {{ $flash }}
        </div>
    @endif
    <table id="table" class="table table-striped table-bordered">
        <thead class="thead-dark">
        <tr>
            <th>ID</th>
            <th>Name AR</th>
            <th>Name FR</th>
            <th>Name EN</th>
            <th>Active</th>
            <th style="width: 150px;">Created at</th>
            <th>Action</th>
        </tr>
        </thead>
    </table>
    <div class="text-right" style="margin-top: 20px;">
        <a href="{{ url('admin/settings/category/create') }}" class="btn btn-lg btn-primary">Add Category</a>
    </div>

@endsection

@section('after_script')

    <script src="https://cdn.datatables.net/v/dt/dt-1.10.18/sc-1.5.0/datatables.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                order: [0,'desc'],
                processing: true,
                serverSide: true,
                ajax: "{{ url('/api/admin/category_list') }}",
                columns: [
                    { "data": "id" },
                    { "data": "name_ar" },
                    { "data": "name_fr" },
                    { "data": "name_en" },
                    {
                        "data": "active",
                        "render": function(data){
                            if(data == 0){
                                return '<i class="fa fa-times-circle" title="Verified" style="font-size:20px; color:#e74c3c;"></i>'
                            }else{
                                return '<i class="fa fa-check-circle" title="Verified" style="font-size:20px; color:#badc58;"></i>';
                            }
                            return data;
                        },
                        "searchable": false,
                        "orderable":false
                    },
                    {
                        "data": "created_at",
                        "searchable": false,
                        "orderable":false,
                    },
                    {
                        "data": "action",
                        "searchable": false,
                        "orderable":false,
                    }
                ]
            });

        });

    </script>

@endsection
