<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'districts',
            function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name_fr');
                $table->string('name_ar');
                $table->string('name_en');
                $table->boolean('active')->default(0);
                $table->timestamps();
            }
        );

        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'affaire relative à tout un secteur',
                'name_ar' => 'شأن يهم قطاع كامل',
                'name_en' => 'A whole sector matter',
                'active' => 1,
            ]
        );

        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Affaire concernant tout les tunisiens',
                'name_ar' => 'شأن يهم كل التونسيين',
                'name_en' => 'a whole tunisian matter',
                'active' => 1,
            ]
        );

        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'France 1',
                'name_ar' => 'فرنسا 1',
                'name_en' => 'France 1',
                'active' => 1,
            ]
        );

        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'France 2',
                'name_ar' => 'فرنسا 2',
                'name_en' => 'France 2',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Italie',
                'name_ar' => 'إيطاليا',
                'name_en' => 'Italy',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Allemagne',
                'name_ar' => 'ألمانيا',
                'name_en' => 'Germany',
                'active' => 1,
            ]
        );

        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Amériques et reste Europe',
                'name_ar' => 'القارة الأمريكية وبقية الدول الأوروبية',
                'name_en' => 'Americas and rest of Europe',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Pays arabes et autres',
                'name_ar' => 'الدول العربية و بقية دول العالم',
                'name_en' => 'Arab countries and others',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Tunis 1',
                'name_ar' => '1 تونس',
                'name_en' => 'Tunis 1',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Tunis 2',
                'name_ar' => 'تونس 2',
                'name_en' => 'Tunis 2',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Ariana',
                'name_ar' => 'أريانة',
                'name_en' => 'Ariana',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'La Mannouba',
                'name_ar' => 'منوبة',
                'name_en' => 'La Mannouba',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Ben Arous',
                'name_ar' => 'بن عروس',
                'name_en' => 'Ben Arous',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Bizerte',
                'name_ar' => 'بنزرت',
                'name_en' => 'Bizerte',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Nabeul 1',
                'name_ar' => 'نابل 1',
                'name_en' => 'Nabeul 1',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Nabeul 2',
                'name_ar' => 'نابل 2',
                'name_en' => 'Nabeul 2',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Zaghouan',
                'name_ar' => 'زغوان',
                'name_en' => 'Zaghouan',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Béja',
                'name_ar' => 'باجـة',
                'name_en' => 'Béja',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'le Kef',
                'name_ar' => 'الكاف',
                'name_en' => 'le Kef',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Siliana',
                'name_ar' => 'سليانة',
                'name_en' => 'Siliana',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Jendouba',
                'name_ar' => 'جندوبة',
                'name_en' => 'Jendouba',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Kairouan',
                'name_ar' => 'القيروان',
                'name_en' => 'Kairouan',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Sousse',
                'name_ar' => 'سوسة',
                'name_en' => 'Sousse',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Monastir',
                'name_ar' => 'المنستير',
                'name_en' => 'Monastir',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Mahdia',
                'name_ar' => 'المهدية',
                'name_en' => 'Mahdia',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Kasserine',
                'name_ar' => 'القصرين',
                'name_en' => 'Kasserine',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Sidi Bouzid',
                'name_ar' => 'سيدي بوزيد',
                'name_en' => 'Sidi Bouzid',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Gafsa',
                'name_ar' => 'قفصة',
                'name_en' => 'Gafsa',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Tozeur',
                'name_ar' => 'توزر',
                'name_en' => 'Tozeur',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Sfax 1',
                'name_ar' => 'صفاقس 1',
                'name_en' => 'Sfax 1',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Sfax 2',
                'name_ar' => 'صفاقس 2',
                'name_en' => 'Sfax 2',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Gabès',
                'name_ar' => 'قابس',
                'name_en' => 'Gabès',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Medenine',
                'name_ar' => 'مدنين',
                'name_en' => 'Medneen',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Tatouine',
                'name_ar' => 'تطاوين',
                'name_en' => 'Tatouine',
                'active' => 1,
            ]
        );
        \Illuminate\Support\Facades\DB::table('districts')->insert(
            [
                'name_fr' => 'Kebili',
                'name_ar' => 'قبلي',
                'name_en' => 'Kebili',
                'active' => 1,
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
