<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /**
         * Super Admin
         */
        Gate::define(
            'super_admin',
            function ($user) {
                return ($user->role->super_admin == 1);
            }
        );

        /*
         * Begin Settings
         */
        Gate::define(
            'settings',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->settings);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'settings.is_visible',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->settings->is_visible);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'settings.is_create',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->settings->is_create);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'settings.is_read',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->settings->is_read);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'settings.is_edit',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->settings->is_edit);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'settings.is_delete',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->settings->is_delete);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );

        Gate::define(
            'user_managemen_roles',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = (isset($permissions->user_management)) || (isset($permissions->roles));
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );

        /*
         * Begin User Management
         */
        Gate::define(
            'user_management',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->user_management);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'user_management.is_visible',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->user_management->is_visible);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'user_management.is_create',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->user_management->is_create);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'user_management.is_read',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->user_management->is_read);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'user_management.is_edit',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->user_management->is_edit);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'user_management.is_delete',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->user_management->is_delete);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );

        /*
         * Begin Roles
         */
        Gate::define(
            'roles',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->roles);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'roles.is_visible',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->roles->is_visible);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'roles.is_create',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->roles->is_create);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'roles.is_read',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->roles->is_read);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'roles.is_edit',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->roles->is_edit);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'roles.is_delete',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->roles->is_delete);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );

        /*
         * Begin Gov Request
         */
        Gate::define(
            'gov_request',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->gov_request);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'gov_request.is_visible',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->gov_request->is_visible);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'gov_request.is_create',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->gov_request->is_create);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'gov_request.is_read',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->gov_request->is_read);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'gov_request.is_edit',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->gov_request->is_edit);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'gov_request.is_delete',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->gov_request->is_delete);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );

        /*
         * Begin Gov Request
         */
        Gate::define(
            'applicant',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->applicant);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'applicant.is_visible',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->applicant->is_visible);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'applicant.is_create',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->applicant->is_create);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'applicant.is_read',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->applicant->is_read);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'applicant.is_edit',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->applicant->is_edit);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
        Gate::define(
            'applicant.is_delete',
            function ($user) {
                if ($user->role->permissions) {
                    $permissions = json_decode($user->role->permissions);
                    $res = isset($permissions->applicant->is_delete);
                } else {
                    $res = false;
                }
                return ($res || $user->role->super_admin == 1);
            }
        );
    }

}
