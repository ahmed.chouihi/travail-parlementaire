<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Request extends Model
{


    protected $fillable = [
        'key',
        'applicant_id',
        'category_id',
        'district_id',
        'user_id',
        'status_id',
        'title',
        'description',
        'request_information',
        'closed',
        'proof_file'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id', 'id');
    }

    public function applicant()
    {
        return $this->belongsTo(Applicant::class, 'applicant_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'request_id', 'id');
    }

    public function gov_reqs()
    {
        return $this->belongsToMany(
            GovRequest::class,
            'gov_requests_requests',
            'request_id',
            'gov_request_id',
            'id',
            'id'
        );
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = Crypt::encryptString($value);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = Crypt::encryptString($value);
    }

    public function getTitleAttribute($value)
    {
        return Crypt::decryptString($value);
    }

    public function getDescriptionAttribute($value)
    {
        return Crypt::decryptString($value);
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['closed'])) {
            if ($closed = $filters['closed']) {
                if ($closed == 'open' || $closed == 'closed') {
                    if ($closed == 'open') {
                        $closed = 0;
                    } else {
                        $closed = 1;
                    }
                    $query->where('closed', $closed);
                }
            }
        }

        if (isset($filters['status'])) {
            if ($status = $filters['status']) {
                if ($status == 'waiting') {
                    $query->where('status_id', null);
                }
                if ($status != '' && $status != 'waiting') {
                    $query->where('status_id', $status);
                }
            }
        }

        if (isset($filters['district'])) {
            if ($district = $filters['district']) {
                if ($district != '') {
                    $query->where('district_id', $district);
                }
            }
        }

        if (isset($filters['user'])) {
            if ($user = $filters['user']) {
                if ($user != '') {
                    $query->where('user_id', $user);
                }
            }
        }

        if (isset($filters['applicant'])) {
            if ($applicant = $filters['applicant']) {
                if ($applicant != '') {
                    $query->where('applicant_id', $applicant);
                }
            }
        }

        if (isset($filters['date'])) {
            if ($date = $filters['date']) {
                if ($date != '') {
                    $date = explode('~', $date);
                    $start = str_replace(" ", "", $date[0]);
                    $end = str_replace(" ", "", $date[1]);
                    $query->where('created_at', '>=', $start)->where('created_at', '<=', $end);
                }
            }
        }

        if (isset($filters['hasGovReq'])) {
            if ($hasGovReq = $filters['hasGovReq']) {
                if ($hasGovReq != '') {
                    if ($hasGovReq == 'yes') {
                        $query->whereHas('gov_reqs');
                    } else {
                        $query->whereDoesntHave('gov_reqs');
                    }
                }
            }
        }
    }

}
